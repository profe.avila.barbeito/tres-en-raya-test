import java.util.Scanner;

public class Main {
    static char[][] tablero = new char[3][3];
    static final char FICHA_VACIA = '_';
    static final int POS_ERROR = 42;
    static final int CONTADOR_VICTORIA = 3;
    static final char EMPATE = 'E';
    static final char VICTORIA = 'V';
    static final char CONTINUAR = 'C';
    static final String MENSAJE_POS_INVALIDA = "Esa posicion no era valida, vuelve a probar";

    public static void main(String[] args) {
        final char FICHA_J1 = 'X';
        final char FICHA_J2 = 'O';

        rellenaTablero();
        boolean turnoJ1 = true;
        char fichaJugadorActual = decideFicha(turnoJ1, FICHA_J1, FICHA_J2);
        char letraFin = CONTINUAR;

        while(letraFin==CONTINUAR){

            fichaJugadorActual = decideFicha(turnoJ1, FICHA_J1, FICHA_J2);
            muestraTablero(tablero);
            System.out.println("Turno del jugador ("+fichaJugadorActual+"):");
            int posX = getPosicion();
            int posY = getPosicion();

//          Version alternativa sin usar continue;
//            if (posY == POS_ERROR || posX == POS_ERROR){
//                System.err.println(MENSAJE_POS_INVALIDA);
//            }else{
//                if (!pintaFicha(fichaJugadorActual, posX, posY)){
//                    System.err.println(MENSAJE_POS_INVALIDA);
//                }else{
//                    turnoJ1 = !turnoJ1;
//                }
//            }

            if (posY == POS_ERROR || posX == POS_ERROR){
                System.err.println(MENSAJE_POS_INVALIDA);
                continue;
            }
            if (!pintaFicha(fichaJugadorActual, posX, posY)){
                System.err.println(MENSAJE_POS_INVALIDA);
                continue;
            }

            turnoJ1 = !turnoJ1;
            letraFin=fin(fichaJugadorActual);
        }

        muestraTablero(tablero);
        if (letraFin==EMPATE){
            System.out.println("Nadie ha ganado :(");
        }else{
            System.out.println("Felicidades ("+fichaJugadorActual+")!!! :)");
        }
    }

    private static char decideFicha(boolean turnoJ1, char fichaJ1, char fichaJ2) {
        if (turnoJ1){
            return fichaJ1;
        }else{
            return fichaJ2;
        }
    }

    private static char fin(char fichaJugadorActual) {

        if (compruebaHorizontal(fichaJugadorActual)){
            return VICTORIA;
        }

        if (compruebaVertical(fichaJugadorActual)){
            return VICTORIA;
        }

        if (compruebaDiagonal(fichaJugadorActual)){
            return VICTORIA;
        }

        if(compruebaEmpate()){
            return EMPATE;
        }

        return CONTINUAR;
    }

    private static boolean compruebaEmpate() {
        for (int i=0; i<tablero.length; i++){
            for (int j=0; j<tablero.length; j++){
                if(tablero[i][j] == FICHA_VACIA){
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean compruebaDiagonal(char fichaJugadorActual) {
        int contador = 0;

        for (int i = 0; i<tablero.length; i++){

            if(tablero[i][i]==fichaJugadorActual){
                contador++;
            }

            if (contador== CONTADOR_VICTORIA){
                return true;
            }
        }
        contador = 0;
//        Version alternativa de la diagonal derecha
//        int j = tablero.length-1;
//        for (int i = 0; i<tablero.length; i++){
//                if(tablero[i][j]==fichaJugadorActual){
//                    contador++;
//                }
//                if (contador==VICTORIA){
//                    return true;
//                }
//                j--;
//        }
        for (int i = 0; i<tablero.length; i++){
            if(tablero[i][tablero.length-1-i]==fichaJugadorActual){
                contador++;
            }
            if (contador== CONTADOR_VICTORIA){
                return true;
            }
        }
        return false;
    }

    private static boolean compruebaVertical(char fichaJugadorActual) {
        for (int i = 0; i<tablero.length; i++){
            int contador = 0;

            for (int j=0; j<tablero.length; j++){
                if(tablero[j][i]==fichaJugadorActual){
                    contador++;
                }
                if (contador== CONTADOR_VICTORIA){
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean compruebaHorizontal(char fichaJugadorActual) {
        for (int i = 0; i<tablero.length; i++){
            int contador = 0;

            for (int j=0; j<tablero.length; j++){
                if(tablero[i][j]==fichaJugadorActual){
                    contador++;
                }
                if (contador== CONTADOR_VICTORIA){
                    return true;
                }
            }
        }
        return false;
    }

    private static int getPosicion() {
        final int POS_MINIMA = 0;
        final int POS_MAX = tablero.length-1;

        System.out.println("Inserte un numero entre ("+POS_MINIMA+" - "+POS_MAX+")");
        Scanner sc = new Scanner(System.in);
        int pos = sc.nextInt();

        if (pos>=POS_MINIMA && pos<=POS_MAX){
            return pos;
        }else{
            return POS_ERROR;
        }
    }

    private static boolean pintaFicha(char ficha, int posX, int posY) {
        if (tablero[posX][posY]==FICHA_VACIA){
            tablero[posX][posY] = ficha;
            return true;
        }
        return false;
    }

    private static void rellenaTablero() {
        for (int i=0; i<tablero.length; i++){
            for (int j=0; j<tablero.length; j++){
                tablero[i][j] = FICHA_VACIA;
            }
        }
    }

    private static void muestraTablero(char[][] tablero) {
        for (int i=0; i<tablero.length; i++){
            for (int j=0; j<tablero.length; j++){
                System.out.print("\t"+tablero[i][j]);
            }
            System.out.println();
        }
    }
}